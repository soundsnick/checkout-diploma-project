package com.example.demo.model;


import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

@Entity
public class Pin {
    private int id;
    private String title;
    private String location;
    private String description;
    private String image;

    private String tag;

    private String color;

    private Client client;

    public @Id @GeneratedValue(strategy = GenerationType.AUTO) int getId() { return id; }
    public void setId(int id) { this.id = id; }

    public String getTitle() { return this.title; }
    public void setTitle(String name) { this.title = name; }

    public String getTag() { return this.tag; }
    public void setTag(String tag) { this.tag = tag; }

    public String getColor() { return this.color; }
    public void setColor(String color) { this.color = color; }

    public String getLocation() { return this.location; }
    public void setLocation(String location) { this.location = location; }

    public String getDescription() { return this.description; }
    public void setDescription(String description) { this.description = description; }

    public String getImage() { return this.image; }
    public void setImage(String image) { this.image = image; }

    @ManyToOne
    @JsonBackReference
    public Client getClient() { return this.client; }
    public void setClient(Client client) { this.client = client; }

    public void fill(String title, String location, String description, String image, String tag, String color, Client client) {
        this.title = title;
        this.tag = tag;
        this.color = color;
        this.location = location;
        this.description = description;
        this.image = image;
        this.client = client;
    }
}