package com.example.demo.model;


import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Entity(name = "client")
public class Client {
    private int id;
    private String firstName;
    private String lastName;
    private String username;
    private String password;

    private Integer steps = 0;

    private List<Pin> pins = new ArrayList<>();

    private List<Seance> seances = new ArrayList<>();

    public @Id @GeneratedValue(strategy = GenerationType.AUTO) int getId() { return id; }
    public void setId(int id) { this.id = id; }

    public String getFirstName() { return this.firstName; }
    public void setFirstName(String firstName) { this.firstName = firstName; }

    public String getLastName() { return this.lastName; }
    public void setLastName(String lastName) { this.lastName = lastName; }

    public String getUsername() { return this.username; }
    public void setUsername(String username) { this.username = username; }

    public String getPassword() { return this.password; }
    public void setPassword(String password) { this.password = password; }

    public Integer getSteps() { return this.steps; }
    public void setSteps(Integer steps) { this.steps = steps; }

    @OneToMany(mappedBy = "client")
    @JsonManagedReference
    public List<Pin> getPins() { return this.pins; }
    public void setPins(List<Pin> pins) { this.pins = pins; }
    public void addPin(Pin pin) {
        this.pins.add(pin);
    }
    public void removePin(Pin pin) {
        this.pins.remove(pin);
    }

    @OneToMany(mappedBy = "client")
    @JsonManagedReference
    public List<Seance> getSeances() { return this.seances; }
    public void setSeances(List<Seance> seance) { this.seances = seance; }
    public void addSeance(Seance seance) {
        this.seances.add(seance);
    }
    public void removeSeance(Seance seance) {
        this.seances.remove(seance);
    }
}