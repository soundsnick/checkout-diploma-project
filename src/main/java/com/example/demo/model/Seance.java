package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

@Entity(name = "seance")
public class Seance {
    private int id;
    private String token;

    private Client client;

    public @Id
    @GeneratedValue(strategy = GenerationType.AUTO) int getId() { return id; }
    public void setId(int id) { this.id = id; }

    public String getToken() { return this.token; }
    public void setToken(String token) { this.token = token; }

    @ManyToOne
    @JsonBackReference
    public Client getClient() { return this.client; }
    public void setClient(Client client) { this.client = client; };
}
