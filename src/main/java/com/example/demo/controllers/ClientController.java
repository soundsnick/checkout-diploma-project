package com.example.demo.controllers;

import com.example.demo.model.Seance;
import com.example.demo.model.Client;
import com.example.demo.repositories.SeanceRepository;
import com.example.demo.repositories.ClientRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.security.NoSuchAlgorithmException;
import java.time.LocalTime;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.security.MessageDigest;

@Controller
@RequestMapping("user")
public class ClientController {
    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private SeanceRepository seanceRepository;

    private String generateHash(String username) throws NoSuchAlgorithmException {
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        messageDigest.update((username + LocalTime.now().toString()).getBytes());
        return new String(Base64.getEncoder().encodeToString(messageDigest.digest()));
    }

    @PostMapping("register")
    public ResponseEntity<HashMap<String, Object>> register(@RequestBody Map<String, String> payload) throws NoSuchAlgorithmException {
        if (payload.containsKey("firstName") && payload.containsKey("lastName") && payload.containsKey("username") && payload.containsKey("password")) {
            String firstName = payload.get("firstName");
            String lastName = payload.get("lastName");
            String username = payload.get("username");
            String password = payload.get("password");

            Client foundUser = clientRepository.findByUsername(username);
            if (foundUser == null) {
                Client newUser = new Client();
                newUser.setFirstName(firstName);
                newUser.setUsername(username);
                newUser.setLastName(lastName);
                newUser.setPassword(password);
                clientRepository.save(newUser);

                String stringHash = generateHash(newUser.getUsername());
                Seance newSession = new Seance();
                newSession.setToken(stringHash);
                newSession.setClient(newUser);
                seanceRepository.save(newSession);

                newUser.addSeance(newSession);
                clientRepository.save(newUser);
                HashMap<String, Object> response = new HashMap<>();
                response.put("token", newSession.getToken());
                return ResponseEntity.status(HttpStatus.OK).body(response);

            } else {
                HashMap<String, Object> response = new HashMap<>();
                response.put("message", "Already registered");
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(response);
            }
        } else {
            HashMap<String, Object> response = new HashMap<>();
            response.put("message", "Fill all the blanks");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }
    }

    @PostMapping("login")
    public ResponseEntity<HashMap<String, Object>> auth(@RequestBody Map<String, String> payload) throws NoSuchAlgorithmException {
        String username = payload.get("username");
        String password = payload.get("password");
        Client foundUser = clientRepository.findByUsernameAndPassword(username, password);
        if (foundUser != null) {
            String stringHash = generateHash(foundUser.getUsername());
            Seance newSession = new Seance();
            newSession.setToken(stringHash);
            newSession.setClient(foundUser);
            seanceRepository.save(newSession);

            foundUser.addSeance(newSession);
            clientRepository.save(foundUser);
            HashMap<String, Object> response = new HashMap<>();
            response.put("token", newSession.getToken());
            return ResponseEntity.status(HttpStatus.OK).body(response);
        } else {
            HashMap<String, Object> response = new HashMap<>();
            response.put("message", "Incorrect username or password");
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(response);
        }
    }

    @GetMapping("steps")
    public ResponseEntity<String> steps(@RequestHeader Map<String, String> headers) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        if (headers.containsKey("authorization")) {
            String[] tokenArr = headers.get("authorization").split(" ");
            if (tokenArr.length > 0 && tokenArr[1] != null) {
                Seance session = seanceRepository.findByToken(tokenArr[1]);
                if (session != null) {
                    return ResponseEntity.status(HttpStatus.OK).body(mapper.writeValueAsString(session.getClient().getSteps()));
                }
            }
        }
        HashMap<String, String> response = new HashMap();
        response.put("message", "unauthorized");
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mapper.writeValueAsString(response));
    }

    @PutMapping("steps")
    public ResponseEntity<String> syncSteps(@RequestHeader Map<String, String> headers, @RequestBody Map<String, Integer> payload) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        if (headers.containsKey("authorization")) {
            String[] tokenArr = headers.get("authorization").split(" ");
            if (tokenArr.length > 0 && tokenArr[1] != null) {
                Seance session = seanceRepository.findByToken(tokenArr[1]);
                if (session != null) {
                    if (payload.containsKey("steps")) {
                        Client client = session.getClient();
                        client.setSteps(payload.get("steps"));
                        clientRepository.save(client);
                        return ResponseEntity.status(HttpStatus.OK).body(mapper.writeValueAsString(client.getSteps()));
                    }
                    HashMap<String, String> response = new HashMap();
                    response.put("message", "steps field must be in data");
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(mapper.writeValueAsString(response));
                }
            }
        }
        HashMap<String, String> response = new HashMap();
        response.put("message", "unauthorized");
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mapper.writeValueAsString(response));
    }
}
