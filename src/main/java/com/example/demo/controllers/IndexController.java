package com.example.demo.controllers;

import com.example.demo.repositories.PinRepository;
import com.example.demo.repositories.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.HashMap;


@RestController
@RequestMapping("")
public class IndexController {
    @Autowired
    PinRepository medicamentRepository;
    @Autowired
    ClientRepository userRepository;

    @GetMapping()
    public HashMap<String, String> get(Model model, HttpSession session) {
        HashMap<String, String> response = new HashMap<>();
        response.put("project_name", "Checkout API");

        return response;
    }
}
