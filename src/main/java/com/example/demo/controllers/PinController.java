package com.example.demo.controllers;

import com.example.demo.model.Pin;
import com.example.demo.model.Seance;
import com.example.demo.model.Client;
import com.example.demo.repositories.PinRepository;
import com.example.demo.repositories.SeanceRepository;
import com.example.demo.repositories.ClientRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("pin")
public class PinController {
    @Autowired
    PinRepository pinRepository;
    @Autowired
    ClientRepository userRepository;

    @Autowired
    SeanceRepository seanceRepository;

    public Client isAuthenticated(Map<String, String> headers) {
        if (headers.containsKey("authorization")) {
            String[] tokenArr = headers.get("authorization").split(" ");
            if (tokenArr.length > 0 && tokenArr[1] != null) {
                Seance session = seanceRepository.findByToken(tokenArr[1]);
                if (session != null) {
                    return session.getClient();
                }
            }
        }
        return null;
    }

    @PostMapping("")
    public ResponseEntity<String> create(@RequestBody Map<String, String> payload, @RequestHeader Map<String, String> headers) throws JsonProcessingException {
        Client user = this.isAuthenticated(headers);
        ObjectMapper mapper = new ObjectMapper();

        if (user != null) {
            if (payload.containsKey("title") && payload.containsKey("tag") && payload.containsKey("color") && payload.containsKey("location") && payload.containsKey("description") && payload.containsKey("image")) {
                Pin pin = new Pin();
                pin.fill(payload.get("title"), payload.get("location"), payload.get("description"), payload.get("image"), payload.get("tag"), payload.get("color"), user);
                pinRepository.save(pin);
                return ResponseEntity.status(HttpStatus.OK).body(mapper.writeValueAsString(pin));
            } else {
                HashMap<String, String> response = new HashMap();
                response.put("message", "Fill all the blanks");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(mapper.writeValueAsString(response));
            }
        } else {
            HashMap<String, String> response = new HashMap();
            response.put("message", "unauthorized");
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mapper.writeValueAsString(response));
        }
    }

    @GetMapping("/list")
    public ResponseEntity<String> list(@RequestHeader Map<String, String> headers) throws JsonProcessingException {
        Client user = this.isAuthenticated(headers);
        ObjectMapper mapper = new ObjectMapper();

        if (user != null) {
            return ResponseEntity.status(HttpStatus.OK).body(mapper.writeValueAsString(user.getPins()));
        } else {
            HashMap<String, String> response = new HashMap();
            response.put("message", "unauthorized");
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mapper.writeValueAsString(response));
        }
    }

    @GetMapping("{id}")
    public ResponseEntity<String> get(@PathVariable int id, @RequestHeader Map<String, String> headers) throws JsonProcessingException {
        Client user = this.isAuthenticated(headers);
        ObjectMapper mapper = new ObjectMapper();

        if (user != null) {
            Optional<Pin> pin = pinRepository.findById(id);
            if (pin.isPresent() && pin.get().getClient().equals(user)) {
                return ResponseEntity.status(HttpStatus.OK).body(mapper.writeValueAsString(pin.get()));
            }
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(mapper.writeValueAsString(new HashMap<>()));
        } else {
            HashMap<String, String> response = new HashMap();
            response.put("message", "unauthorized");
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mapper.writeValueAsString(response));
        }
    }

    @DeleteMapping("{id}")
    public ResponseEntity<String> delete(@PathVariable int id, @RequestHeader Map<String, String> headers) throws JsonProcessingException {
        Client user = this.isAuthenticated(headers);
        ObjectMapper mapper = new ObjectMapper();

        if (user != null) {
            Optional<Pin> pin = pinRepository.findById(id);
            if (pin.isPresent() && pin.get().getClient().equals(user)) {
                pinRepository.deleteById(id);
                HashMap<String, String> response = new HashMap<>();
                response.put("message", "ok");
                return ResponseEntity.status(HttpStatus.OK).body(mapper.writeValueAsString(response));
            }
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(mapper.writeValueAsString(new HashMap<>()));
        } else {
            HashMap<String, String> response = new HashMap();
            response.put("message", "unauthorized");
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mapper.writeValueAsString(response));
        }
    }

    @PatchMapping("{id}")
    public ResponseEntity<String> update(@PathVariable int id, @RequestBody Map<String, String> payload, @RequestHeader Map<String, String> headers) throws JsonProcessingException {
        Client user = this.isAuthenticated(headers);
        ObjectMapper mapper = new ObjectMapper();

        if (user != null) {
            Optional<Pin> pin = pinRepository.findById(id);
            if (pin.isPresent() && pin.get().getClient().equals(user)) {

                Pin pinItem = pin.get();
                if (payload.containsKey("title")) {
                    pinItem.setTitle(payload.get("title"));
                }
                if (payload.containsKey("description")) {
                    pinItem.setTitle(payload.get("description"));
                }
                if (payload.containsKey("image")) {
                    pinItem.setTitle(payload.get("image"));
                }
                if (payload.containsKey("tag")) {
                    pinItem.setTitle(payload.get("tag"));
                }
                if (payload.containsKey("color")) {
                    pinItem.setTitle(payload.get("color"));
                }
                if (payload.containsKey("location")) {
                    pinItem.setTitle(payload.get("location"));
                }
                pinRepository.save(pinItem);
                return ResponseEntity.status(HttpStatus.OK).body(mapper.writeValueAsString(pinItem));
            }
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(mapper.writeValueAsString(new HashMap<>()));
        } else {
            HashMap<String, String> response = new HashMap();
            response.put("message", "unauthorized");
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(mapper.writeValueAsString(response));
        }
    }
}
