package com.example.demo.repositories;

import com.example.demo.model.Seance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SeanceRepository extends JpaRepository<Seance, Integer> {
    Seance findByToken(String token);
}
